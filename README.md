
To build an image:
``` bash
docker build -t hello-app .
```

To run the container from the image (hello-app) in the background exposing the port 5000 with a name hello-server (not to confuse with the image name)

```bash
docker run -d -p 5000:5000 --name hello-server hello-app
```

To start the server
```bash

docker start hello-server
```

To execute a command on running server. We login to login the server with the following commands:
* -i - input/output
* -t - a terminal 

```bash

 docker exec -it hello-server bash
```
