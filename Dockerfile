FROM python:3.7.2-slim


RUN mkdir /opt/hello_app

# We are going to start all the command from here
WORKDIR /opt/hello_app

# Will copy the file into workdir
ADD requirements.txt .


RUN pip install -r requirements.txt

# Add all filed to the directory
ADD . .

EXPOSE 5000

# if we have env any flask run command will look for
ENV FLASK_APP=hello.py

# execute at the end - execute the commands. "0.0.0.0" allows for any ip addresses coming to the container to connect to exposed
CMD ["flask", "run", "--host", "0.0.0.0"]
