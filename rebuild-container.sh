#!/usr/bin/env bash

imageName=hello-app
containerName=hello-server

echo "Stopping container..."
docker stop $containerName

echo "Delete old container..."
docker rm -f $containerName

docker build -t $imageName -f Dockerfile  .


echo "Run new container..."
docker run -d -p 5000:5000 --name $containerName $imageName